//
//  SLCarData.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 01/10/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import Foundation
import Alamofire

class SLCarData {
    
    final let urlCars:String = "https://desafiobrq.herokuapp.com/v1/carro/" //"http://www.mocky.io/v2/5bc79b79320000540059fbae" 
    
    
    func getCars(onComplete: @escaping ([Carros]?) -> Void){
        Alamofire.request(urlCars).responseJSON { (response) in
            guard let data = response.data, response.result.isSuccess else{
                    onComplete(nil)
                    return
            }
            do{
                let listCars = try JSONDecoder().decode([Carros].self, from: data)
                onComplete(listCars)
            }catch{
                onComplete(nil)
            }
        }
    }
    
    func getDetailsCar(id: Int, onComplete: @escaping (Carros?) -> Void){
        Alamofire.request("\(urlCars)\(String(id))").responseJSON { (response) in
            guard let data = response.data,response.result.isSuccess else{
                    onComplete(nil)
                    return
            }
            do{
                let car = try JSONDecoder().decode(Carros.self, from: data)
                onComplete(car)
            }catch{
               onComplete(nil)
            }
        }
    }
}
