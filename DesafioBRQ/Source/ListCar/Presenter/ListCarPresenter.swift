//
//  ListCarPresenter.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 02/10/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import Foundation

protocol CarViewDelegate {
    func startLoading()
    func stopLoading()
    func errorService()
    func setCars(cars: [Carros])
}
protocol CartDelegate {
    func isEmpty()
}

class ListCarPresenter {
    
    private let carService: SLCarData
    private var carView : CarViewDelegate?
    var delegate : CartDelegate?
    
    init(carService: SLCarData){
        self.carService = carService
    }
    
    func attachView(viewCar: CarViewDelegate){
        carView = viewCar
    }
    
    func getAllCars(){
        self.carView?.startLoading()
        carService.getCars {
            (cars) in
                if let cars = cars, cars.count > 0{
                    DispatchQueue.main.async {
                        self.carView?.setCars(cars: cars)
                        self.carView?.stopLoading()
                    }
            }else{
                self.carView?.errorService()
            }
        }
    }
    func verifyCart<T>(carrinho: [Carros], function: @autoclosure () -> T) {
        if carrinho.isEmpty {
            self.delegate?.isEmpty()
        }else {
            let _: T = function()
        }
    }
}
