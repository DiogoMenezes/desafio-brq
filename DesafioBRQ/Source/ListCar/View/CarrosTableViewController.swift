//
//  CarrosTableViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 8/16/18.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit
import SkeletonView
import Spruce

protocol ListCarDelegate {
    func showDetailsCar(index:Int)
    func buyCar(index: Int)
}

protocol BuyCarDelegate {
    func actionCarBuy()
    func showCart()
}

protocol ResetArrayDelegate {
    func resetArray()
}

class CarrosTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var indexOfRow = 0
    var carros : [Carros] = []
    var carsCart : [Carros] = []
    var isError: Bool = false
    var isLoading: Bool = false
    @IBOutlet weak var refreshControl: UIRefreshControl!
    //var refreshControl: UIRefreshControl?
    @IBOutlet var tableViewCars: UITableView!
    private let carPresenter:ListCarPresenter = ListCarPresenter(carService: SLCarData())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addRefreshControl()
        carPresenter.attachView(viewCar: self)
        carPresenter.getAllCars()
        registerXibs()
        
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isLoading ? 15 : isError ? 1 : carros.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoading {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCar", for: indexPath) as! CarrosTableViewCell
            cell.showAnimatedGradientSkeleton()
            //cell.selectionStyle = .none
            return cell
        }else if isError{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellError", for: indexPath)
            cell.spruce.animate([.fadeIn, .expand(.moderately)])
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCar", for: indexPath) as! CarrosTableViewCell
            let carro = carros[indexPath.row]
            cell.hideSkeleton()
            cell.prepare(with: carro)
            cell.index = indexPath.row
            cell.delegate = self
            //cell.selectionStyle = .none
            //cell.spruce.animate([.spin(.moderately)])
            return cell
        }
    }
    
    func addRefreshControl(){
        self.refreshControl.tintColor = UIColor.red
        self.refreshControl.addTarget(self, action: #selector(refreshList), for:.valueChanged)
    }
    
    @objc func refreshList(){
        carPresenter.getAllCars()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as? CarsDetailViewController
        let destination = carros[self.indexOfRow]
        controller?.carros = destination
        controller?.delegate = self
        controller?.index = destination.id! // modificacao realizada
    }
    
    func registerXibs(){
        tableViewCars.register(UINib(nibName: "CarrosTableViewCell", bundle: nil), forCellReuseIdentifier: "cellCar")
        tableViewCars.register(UINib(nibName: "errorService", bundle: nil), forCellReuseIdentifier: "cellError")
    }
}

extension CarrosTableViewController: ListCarDelegate {
    
    func showDetailsCar(index: Int) {
        self.indexOfRow = index
        self.performSegue(withIdentifier: "DetailsCar", sender: nil)
    }
    
    func buyCar(index: Int) {
        self.indexOfRow = index
        let car = carros[index]
        verifyCarsInCart(car: car)
        popupMessage(title: "SUCESSO", message: "Carro adicionado ao carrinho com sucesso!")
    }
    
    private func verifyCarsInCart(car: Carros) {
            self.carsCart.append(car)
    }
}

extension CarrosTableViewController: BuyCarDelegate {
    @IBAction func buttonCart(_ sender: Any) {
            showCart()
    }
    
    func actionCarBuy() {
        buyCar(index: self.indexOfRow)
    }
    
    func showCart() {
        self.carPresenter.delegate = self
        carPresenter.verifyCart(carrinho: carsCart, function: setCart())
    }
    
    func setCart(){
        let view = UIStoryboard.init(name:"Main", bundle:nil).instantiateViewController(withIdentifier:"cartView") as! ShoppingCartViewController
        view.setCarsCart(cars: self.carsCart)
        view.deleteCar = {(index) -> Void in self.carsCart.remove(at: index)}
        self.navigationController?.pushViewController(view, animated: true)
        view.delegate = self
    }
 
}

extension CarrosTableViewController: CarViewDelegate {
    
    func startLoading() {
        self.isLoading = true
        self.isError = false
        self.tableViewCars.reloadData()
    }
    
    func stopLoading() {
        self.isLoading = false
        self.tableViewCars.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    func errorService() {
        self.isError = true
        stopLoading()
    }
    
    func setCars(cars: [Carros]) {
        self.carros = cars
    }
    
}

extension CarrosTableViewController: ResetArrayDelegate {
    func resetArray() {
        self.carsCart.removeAll()
    }
}

extension CarrosTableViewController: CartDelegate {
    func isEmpty() {
        popupMessage(title: "ERRO", message: "Não há nenhum carro na lista do carrinho!!")
    }
    

}
