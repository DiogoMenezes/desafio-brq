//
//  CarsDetailViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 17/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

class CarsDetailViewController: UIViewController {
    
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var buttonBuyCar: UIButton!
    @IBOutlet weak var detailsView: UIView!
    
    
    var carros:Carros!
    private let carDetailsPresenter:CarDetailsPresenter = CarDetailsPresenter(carService: SLCarData())
    var index: Int = 0
    var delegate: BuyCarDelegate?
    var isError: Bool = false
    
    @IBAction func buttonShowCart(_ sender: Any) {
        delegate?.showCart()
    }
    
    @IBAction func actionCarBuy(_ sender: Any) {
        delegate?.actionCarBuy()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carDetailsPresenter.attachView(carDetails: self)
        carDetailsPresenter.getChosenCar(index: index)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        carDetailsPresenter.delegate = self
       self.carDetailsPresenter.popError(isError)
    }

    func showCar(){
            let url = URL(string: self.carros.imagem)
            imageDetail?.kf.indicatorType = .activity
            imageDetail?.kf.setImage(with: url)
            labelPrice.text = String(format: "R$ %.2f", self.carros.preco!).replacingOccurrences(of: ".", with: ",")
            textViewDescription.text = self.carros.descricao!
            if let quantCars = self.carros.quantidade {
                labelQuantity.text = "Qtd: \(String(quantCars))"
            }
    }
}

extension CarsDetailViewController: CarDetailsViewDelegate{
    func startLoading() {
        self.detailsView.showAnimatedGradientSkeleton()
    }
    
    func stopLoading() {
        self.detailsView.hideSkeleton()
    }
    
    func errorService() {
        isError = true
        popupMessage(title: "Erro", message: "Erro de serviço!!! Voltando para tela inicial. ")
    }
    
    func setDetailsCar(car: Carros, index:Int) {
        self.carros = car
        self.index = index
        self.showCar()
    }
}

extension CarsDetailViewController: PopUpReturnViewDelegate {
    func returnMainView() {
        let viewControllers = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[0], animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}

