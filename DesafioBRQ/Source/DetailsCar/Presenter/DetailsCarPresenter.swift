//
//  DetailsCarPresenter.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 02/10/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import Foundation
import UIKit

protocol CarDetailsViewDelegate {
    func startLoading()
    func stopLoading()
    func errorService()
    func setDetailsCar(car: Carros, index: Int)
}

class CarDetailsPresenter {
    
    private let carService: SLCarData
    private var carDetailsView : CarDetailsViewDelegate?
    var delegate: PopUpReturnViewDelegate?
    
    init(carService: SLCarData){
        self.carService = carService
    }
    
    func attachView(carDetails: CarDetailsViewDelegate){
        carDetailsView = carDetails
    }
    
    func getChosenCar(index: Int){
        self.carDetailsView?.startLoading()
        carService.getDetailsCar(id: index) { (car) in
            if let car = car{
                DispatchQueue.main.async {
                    self.carDetailsView?.setDetailsCar(car: car, index: index)
                    self.carDetailsView?.stopLoading()
                }
            }else{
                self.carDetailsView?.errorService()
            }
        }
    }
    
    func popError(_ isError: Bool) {
        if isError {
            self.delegate?.returnMainView()
        }
    }
}
