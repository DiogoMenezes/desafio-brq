//
//  CarrosTableViewCell.swift
//  DesafioBRQ
//
//  Created by  Diogo Menezes on 8/16/18.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit
import Kingfisher

class CarrosTableViewCell: UITableViewCell {
    
    var index: Int = 0
    var delegate: ListCarDelegate?
    @IBOutlet weak var carsImage: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var buttonBuyCar: UIButton!
    @IBOutlet weak var buttonDetailsCar: UIButton!
    
    @IBAction func buttonBuyCar(_ sender: UIButton) {
        delegate?.buyCar(index: self.index)
        
    }
    
    @IBAction func buttonDetailsCar(_ sender: UIButton) {
        delegate?.showDetailsCar(index:self.index)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func prepare(with car: Carros){
        labelName.text = car.nome
        labelPrice.text = String(format: "R$ %.2f", car.preco!)
        let url = URL(string: car.imagem)
        carsImage?.kf.indicatorType = .activity
        carsImage?.kf.setImage(with: url)
    }
}
