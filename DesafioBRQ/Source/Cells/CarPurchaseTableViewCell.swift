//
//  CarPurchaseTableViewCell.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 18/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

class CarPurchaseTableViewCell: UITableViewCell {

    var index: Int = 0
    
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var nameCarCell: UILabel!
    @IBOutlet weak var priceCarCell: UILabel!
    @IBOutlet weak var qtdCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func prepare(index: Int, car: Carros){
        nameCarCell.text = car.nome
        priceCarCell.text = String(format: "R$ %.2f", car.preco!)
        qtdCell.text = String(format: "Quantidade: 1")
        let url = URL(string: car.imagem)
        imageCell.kf.indicatorType = .activity
        imageCell?.kf.setImage(with: url)
    }
    
}
