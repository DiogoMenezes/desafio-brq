//
//  ShoppingCartViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 17/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

class ShoppingCartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var labelShoppingCart: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var tableViewCars: UITableView!
    @IBOutlet weak var buttonFinalizePurchase: UIButton!

    let presenterShoppingCart = ShoppingCartViewPresenter()
    
    var carros: [Carros] = []
    var deleteCar:((_ index:Int)->Void?)?
    var delegate: ResetArrayDelegate?
    var dismissScreenListSuccess:Bool = false
    
    @IBAction func finalizePurchase(_ sender: Any) {
        self.presenterShoppingCart.validateCart(totalValueCart(), tableViewCars)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startValuesComponents()
        registerXib()
        presenterShoppingCart.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenterShoppingCart.popUpReturnView = self
        self.presenterShoppingCart.popFinalizePurchase(dismissScreenListSuccess)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carros.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellcart = tableView.dequeueReusableCell(withIdentifier: "carPurchase", for: indexPath) as! CarPurchaseTableViewCell
        let car = carros[indexPath.row]
        cellcart.prepare(index: indexPath.row, car: car)
        return cellcart
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            self.deleteCar!(indexPath.item)
            self.carros.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            _ = self.totalValueCart()
        }
    }
    
    func totalValueCart() -> Float{
        let total = carros.reduce(0) { (result, car) in
            return result + car.preco!
        }
        self.totalPrice.text = String(format: "Total do carrinho: R$ %.2f", total).replacingOccurrences(of: ".", with: ",")
        return total
    }
    
    func startValuesComponents(){
        self.totalPrice.text = String(format: "Total do carrinho: R$ %.2f", totalValueCart()).replacingOccurrences(of: ".", with: ",")
    }
    
    func setCarsCart(cars:[Carros]){
        self.carros = cars
    }
    
    func registerXib(){
        tableViewCars.register(UINib(nibName:"CarPurchaseTableViewCell", bundle: nil), forCellReuseIdentifier: "carPurchase")
    }
}

extension ShoppingCartViewController: ShoppingCartViewPresenterDelegate {
    func outdatedLimit(_ value: Float) {
        popupMessage(title: "ERRO", message: "O limite máximo é R$ 100.000,00 !!")
    }
    func isEmpty(_ table: UITableView) {
        popupMessage(title: "ERRO", message: "Não há nenhum carro na lista do carrinho!!")
    }
    func sucessBuy() {
        popupMessage(title: "SUCESSO", message: "Compra finalizada com sucesso !!")
        dismissScreenListSuccess = true
        delegate?.resetArray()
    }
}

extension ShoppingCartViewController: PopUpReturnViewDelegate {
    func returnMainView() {
        let viewControllers = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[0], animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}
