//
//  PopupsViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 17/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

protocol PopupDelegate {
    func popupStatus()
}

protocol PopUpReturnViewDelegate {
    func returnMainView()
}

class PopupsViewController: UIViewController {
    
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var titlePopup: UILabel!
    @IBOutlet weak var descriptionPopup: UILabel!
    @IBOutlet weak var buttonPopup: UIButton!
    @IBOutlet var popupView: UIView!
    
    var popupTitle:String?
    var messagePopup:String?
    var delegate:PopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startValuesComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = popupView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        popupView.addSubview(blurEffectView)
        self.popupView.bringSubview(toFront: self.viewPopup)
    }
    
    @IBAction func buttonPopupConfirmPurchase(_ sender: Any) {
        self.delegate?.popupStatus()
    }
    
    func startValuesComponents(){
        if let title = popupTitle {
            titlePopup.text = title.uppercased()
        }
        if let description = messagePopup {
            descriptionPopup.text = description
        }
    }
    
}
