//
//  UIViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 08/10/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//


import UIKit

extension UIViewController: PopupDelegate {
    func popupMessage(title:String, message:String ){
        let controller = UIStoryboard(name: "Popups", bundle: nil).instantiateViewController(withIdentifier: "popupPurchase") as! PopupsViewController
        controller.delegate = self
        controller.popupTitle = title
        controller.messagePopup = message
        self.present(controller, animated: true, completion: nil)
    }
    
    func popupStatus() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func toastMessage(_ message: String){
        let message = message
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        self.present(alert, animated: true)
        let duration: Double = 0.7
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
}
