//
//  Carro.swift
//  DesafioBRQ
//
//  Created by  Diogo Menezes on 8/16/18.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import Foundation

struct Carros: Codable {
    var id: Int?
    var nome: String?
    var descricao:String?
    var preco: Float?
    var marca:String?
    var quantidade:Int?
    var imagem: String
}
