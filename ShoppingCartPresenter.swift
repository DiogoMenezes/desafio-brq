//
//  Teste.swift
//  
//
//  Created by Diogo Menezes on 25/10/2018.
//



import Foundation
import UIKit

protocol ShoppingCartViewPresenterDelegate {
    func outdatedLimit(_ value:Float)
    func isEmpty(_ table: UITableView)
    func sucessBuy()
}

class ShoppingCartViewPresenter {
    var delegate: ShoppingCartViewPresenterDelegate?
    var popUpReturnView : PopUpReturnViewDelegate?
    
    func validateCart(_ value:Float, _ tableView: UITableView) {
        if tableView.visibleCells.isEmpty {
            self.delegate?.isEmpty(tableView)
        }else if value > 100000 {
            self.delegate?.outdatedLimit(value)
        }else{
            self.delegate?.sucessBuy()
        }
    }
    
    func popFinalizePurchase(_ isError: Bool) {
        if isError {
            self.popUpReturnView?.returnMainView()
        }
    }
}

